node default {
  notify { "Debug output on ${hostname} node.": }
}

node 'node01.example.com', 'node02.example.com' {
  notify { "Debug output on ${hostname} node.": }
}
