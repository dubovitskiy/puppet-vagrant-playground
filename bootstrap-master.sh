#!/bin/sh

# Will be executed on master node

# Fail on first error
set -e

# Detect first run and install necessary packages
rpm -q ansible &> /dev/null || sudo yum install -y ansible sshpass &> /dev/null

# Generate ssh keys if it does not exist
if [[ ! -f /home/vagrant/.ssh/id_rsa ]]; then
    echo "Generating ssh key pair"
    # TODO: rewrite in more intelligent way
    echo -e "\n\n\n\n" | ssh-keygen -t rsa -N "" &> /dev/null
fi

# Copy ssh keys to the nodes
sshpass -p vagrant ssh-copy-id -o StrictHostKeyChecking=no vagrant@192.168.32.20 &> /dev/null
sshpass -p vagrant ssh-copy-id -o StrictHostKeyChecking=no vagrant@192.168.32.10 &> /dev/null

# Execute ansible playbook that will install and configure
# puppet server and agents on the virtual machines
ansible-playbook -i /vagrant/ansible/inventory /vagrant/ansible/playbook.yml

PUPPET_MANIFESTS_DIR=/etc/puppetlabs/code/environments/production/manifests
PUPPET_EXECUTABLE=/opt/puppetlabs/bin/puppet

if [[ ! -f "${PUPPET_MANIFESTS_DIR}/site.pp" ]]; then
    echo "Linking main manifest..."
    sudo ln -s /vagrant/site.pp "${PUPPET_MANIFESTS_DIR}/site.pp"
fi

echo "Starting execution of puppet"


# 0: The run succeeded with no changes or failures; the system was already in
#    the desired state.

# 1: The run failed, or wasn't attempted due to another run already in progress.

# 2: The run succeeded, and some resources were changed.

# 4: The run succeeded, and some resources failed.

# 6: The run succeeded, and included both changes and failures.

# Puppet executes successfully exit code 2
sudo $PUPPET_EXECUTABLE agent --verbose --test || exit 0